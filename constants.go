package uri

// byte slice constants
var (
	strSlashSlash  = []byte("//")
	strSlashDotDot = []byte("/..")
	strZone        = []byte("%25")
)
