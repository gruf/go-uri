package uri

import (
	"unsafe"

	"codeberg.org/gruf/go-bytes"
)

// check Bytes implements bytes.Bytes
var _ bytes.Bytes = Bytes{}

// Bytes is the simplest possible implementation of bytes.Bytes,
// with convenient length and capacity methods
type Bytes []byte

func (b Bytes) Len() int {
	return len(b)
}

func (b Bytes) Cap() int {
	return cap(b)
}

func (b Bytes) Bytes() []byte {
	return b
}

func (b Bytes) String() string {
	return string(b)
}

func (b Bytes) StringPtr() string {
	return *(*string)(unsafe.Pointer(&b))
}
